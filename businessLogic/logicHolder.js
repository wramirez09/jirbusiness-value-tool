/*
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// START global variables
// some variables are needed by multiple functions, so we define them only once

// ---------------------------------------------------------
// START parse the value of the input fields

var valueAgeOfIssue = parseFloat($('#ageOfIssue').val());
var valueClientCategory = parseFloat($('#clientCategory').val());
var valueClientsAffected = parseFloat($('#clientsAffected').val());
var valueCustomersAffected = parseFloat($('#customersAffected').val());
var valueCustomersDisturbed = parseFloat($('input[name=custDisturbed]:checked').val());
var valueDeadline = parseFloat($('#deadline').val());
var valueEarnings = parseFloat($('#earnings').val());
var valueEarningsProbability = parseFloat($('input[name=earningsProbability]:checked').val());
var valueInternalCosts = parseFloat($('#internalCosts').val());
var valueIssueType = parseFloat($('input[name=issueType]:checked').val());
var valueReusableElements = parseFloat($('input[name=reusableElements]:checked').val());
var valueRisk = parseFloat($('#risk').val());
var valueSavings = parseFloat($('#savings').val());
var valueStrategy = parseFloat($('input[name=strategy]:checked').val());
var valueUrgent = parseFloat($('input[name=urgent]:checked').val());
var valueWorkaround = parseFloat($('input[name=workaround]:checked').val());

// END parse the value of the input fields
// ---------------------------------------------------------

// ---------------------------------------------------------
// START getIssueID

function getIssueId() {
    var issueId = $("#issueId input").val();
    return issueId;
}

// END getIssueID
// ---------------------------------------------------------


// END global variables
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/
var calculatedBusinessValue = 0;
var calculatedImportanceScore = 0;
var calculatedUrgencyScore = 0; // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// START document.ready part

$(document).ready(function () {
  // ---------------------------------------------------------
  // START transform poor radio buttons into awesome buttons
  $(function () {
    $('input[type=radio]').checkboxradio();
  }); // END transform poor radio buttons into awesome buttons
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START create info popup

  $('.tip').each(function () {
    $(this).tooltip({
      html: true,
      title: $('#' + $(this).data('tip')).html()
    });
  }); // END create info popus
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START update bar when value changes

  $('select').each(function () {
    var dropdownId = $(this).attr('id');
    visualizeCriticality(dropdownId);
    $('#' + dropdownId).change(function () {
      visualizeCriticality(dropdownId);
    });
  }); // END update bar when value changes
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START issueType -> html classes to the body

  $('.issueType').click(function () {
    showInputFields();
  });

  function showInputFields() {
    var IssueType = $('input[name=issueType]:checked').attr('id');
    $('body').removeClass();

    if (IssueType === 'bug') {
      $('body').addClass('issueTypeSelected issueTypeBug calculationStarted');
    } else if (IssueType === 'feature') {
      $('body').addClass('issueTypeSelected issueTypeFeature calculationStarted');
    } else if (IssueType === 'consultation') {
      $('body').addClass('issueTypeSelected issueTypeConsultation calculationStarted');
    }

    ;
    calculateBusinessValue();
  }

  ;
  showInputFields(); // END issueType -> html classes to the body
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START "inputOptions" changed -> calculation business value

  $('.inputOptions').change(function () {
    calculateBusinessValue();
  }); // START "inputOptions" changed -> calculation business value
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START reset button

  $('#resetButton').click(function () {
    var r = confirm('Do you really want to clear all inputs and start again?');

    if (r == true) {
      $('#businessValueTool')[0].reset();
      $('select').trigger('change');
      showDialog('Everything reset succesfully.');
    }
  }); // END reset button
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START save button

  $('#saveButton').click(function () {
    saveResultstoJSON(); //getSelectedOptions();
  }); // END save button
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START API button

  $('#APIButton').click(function () {
    JiraUpdates();
  }); // END API button
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START load button

  $('#loadButton').click(function () {
    loadData();
  }); // END load button
  // ---------------------------------------------------------

  $('#reprioritiseOptions').click(function () {
    console.log(1);
    $('#boxAgeOfIssue').fadeToggle();
  });
}); // END
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// START calculateBusinessValue

function calculateBusinessValue() {
  // ---------------------------------------------------------
  // START parse the value of the input fields
  var valueIssueType = parseFloat($('input[name=issueType]:checked').val());
  var valueAgeOfIssue = parseFloat($('#ageOfIssue').val());
  var valueClientsAffected = parseFloat($('#clientsAffected').val());
  var valueCustomersAffected = parseFloat($('#customersAffected').val());
  var valueCustomersDisturbed = parseFloat($('input[name=custDisturbed]:checked').val());
  var valueDeadline = parseFloat($('#deadline').val());
  var valueEarnings = parseFloat($('#earnings').val()) || 0;
  var valueEarningsProbability = parseFloat($('input[name=earningsProbability]:checked').val()) || 0;
  var valueClientCategory = parseFloat($('#clientCategory').val()); //		var valueInternalCosts = parseFloat($('#internalCosts').val()); 

  var valueInternalCosts = parseFloat($('#taskSize').val());
  var valueReusableElements = parseFloat($('input[name=reusableElements]:checked').val());
  var valueRisk = parseFloat($('#risk').val());
  var valueSavings = parseFloat($('#fee').val());
  var valueStrategy = parseFloat($('input[name=strategy]:checked').val());
  var valueUrgent = parseFloat($('input[name=urgent]:checked').val());
  var valueWorkaround = parseFloat($('input[name=workaround]:checked').val()); // END parse the value of the input fields
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START certain values need to be combined in order to be usable
  // ___earnings need to be combined with their probability

  valueLikelyEarnings = parseFloat(valueEarnings * valueEarningsProbability); // ___ for customers, we need to look at both just affected and really disturbed ones 

  valueTranslatedCustomers = parseFloat(valueCustomersAffected * valueCustomersDisturbed); // END certain values need to be combined in order to be usable
  // ---------------------------------------------------------
  // ---------------------------------------------------------
  // START sum the (translated) values of the inputs and write them into the variable calculatedBusinessValue[Bug/Feature/Consultation]
  // different issue types require different functions since they use different inputs

  var IssueType = $('input[name=issueType]:checked').attr('id');

  if (IssueType === 'bug') {
    // **********************************
    // START Bug Request
    // calculate the importance score for bug requests
    var calculatedImportanceScoreBug = valueIssueType + valueClientCategory + valueTranslatedCustomers + valueInternalCosts + valueClientsAffected + valueSavings; // calculate the urgency score for bug requests

    var calculatedUrgencyScoreBug = valueAgeOfIssue + valueDeadline + valueCustomersDisturbed + valueRisk + valueUrgent + valueWorkaround;
    calculatedImportanceScoreBug = calculatedImportanceScoreBug / 2;
    calculatedUrgencyScoreBug = calculatedUrgencyScoreBug / 2; // sum the importance score and the urgency score in order to get the business value
    //var calculatedBusinessValueBug = calculatedImportanceScoreBug + calculatedUrgencyScoreBug;

    calculatedImportanceScore = calculatedImportanceScoreBug;
    calculatedUrgencyScore = calculatedUrgencyScoreBug; // END Bug Request
    // **********************************
  } else if (IssueType === 'feature') {
    // **********************************
    // START Feature Request
    // calculate the importance score for feature requests
    var calculatedImportanceScoreFeature = valueIssueType + valueClientCategory + valueInternalCosts + valueReusableElements + valueStrategy + valueClientsAffected + valueCustomersAffected + valueLikelyEarnings + valueSavings; // calculate the urgency score for feature requests

    var calculatedUrgencyScoreFeature = valueIssueType + valueAgeOfIssue + valueDeadline + valueUrgent + valueRisk + valueWorkaround;
    calculatedImportanceScoreFeature = calculatedImportanceScoreFeature / 2;
    calculatedUrgencyScoreFeature = calculatedUrgencyScoreFeature / 2; // sum the importance score and the urgency score in order to get the business value
    //var calculatedBusinessValueFeature = calculatedImportanceScoreFeature + calculatedUrgencyScoreFeature;

    calculatedImportanceScore = calculatedImportanceScoreFeature;
    calculatedUrgencyScore = calculatedUrgencyScoreFeature; // END Feature Request
    // **********************************
  } else if (IssueType === 'consultation') {
    // **********************************
    // START Consultation Request
    // calculate the importance score for consultation requests
    var calculatedImportanceScoreConsultation = valueIssueType + valueCustomersAffected + valueInternalCosts; // calculate the urgency score for consultation requests

    var calculatedUrgencyScoreConsultation = valueIssueType + valueCustomersDisturbed + valueAgeOfIssue;
    calculatedImportanceScoreConsultation = calculatedImportanceScoreConsultation / 2;
    calculatedUrgencyScoreConsultation = calculatedUrgencyScoreConsultation / 2; // sum the importance score and the urgency score in order to get the business value
    //var calculatedBusinessValueConsultation = calculatedImportanceScoreConsultation + calculatedUrgencyScoreConsultation;

    calculatedImportanceScore = calculatedImportanceScoreConsultation;
    calculatedUrgencyScore = calculatedUrgencyScoreConsultation; // END Consultation Request
    // **********************************
  }

  ; // END sum the (translated) values of the inputs and write them into the variable calculatedBusinessValue[Bug/Feature/Consultation]
  // ---------------------------------------------------------

  calculatedImportanceScore = Math.round(calculatedImportanceScore);
  calculatedUrgencyScore = Math.round(calculatedUrgencyScore);
  calculatedBusinessValue = calculatedImportanceScore + calculatedUrgencyScore; //    console.log(calculatedImportanceScore);
  //    console.log(calculatedUrgencyScore);

  if (calculatedBusinessValue < 0) {
    calculatedBusinessValue = 0;
  }

  if (calculatedBusinessValue > 100) {
    calculatedBusinessValue = 100;
  }

  $('#businessValue').text(calculatedBusinessValue);
  $('#importanceScore').text(calculatedImportanceScore);
  $('#urgencyScore').text(calculatedUrgencyScore);
}

; // END calculateBusinessValue
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

function showDialog(dialogText) {
  //alert('Results saved!');
  // Get the #dialog div
  var x = document.getElementById('dialog'); // message that will be displayed

  $(x).text(dialogText); // Add the "show" class to #dialog div

  x.className = 'show'; // After 3 seconds, remove the show class from #dialog div

  setTimeout(function () {
    x.className = x.className.replace('show', '');
  }, 3000);
}

; // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// START saveResults
// ---------------------------------------------------------
// START getSelectedOptions

function getSelectedOptions() {
  // ---------------------------------------------------------
  // START parse the value of the input fields
  var valueAgeOfIssue = $('#ageOfIssue :selected').text();
  var valueClientCategory = $('#clientCategory :selected').text();
  var valueClientsAffected = $('#clientsAffected :selected').text();
  var valueCustomersAffected = $('#customersAffected :selected').text();
  var valueCustomersDisturbed = $('input[name=custDisturbed]:checked').attr('id');
  var valueDeadline = $('#deadline :selected').text();
  var valueEarnings = $('#earnings :selected').text();
  var valueEarningsProbability = $('input[name=earningsProbability]:checked').attr('id'); //	var valueInternalCosts = $('#internalCosts :selected').text();

  var valueTaskSize = $('#taskSize :selected').text();
  var valueIssueType = $('input[name=issueType]:checked').attr('id');
  var valueReusableElements = $('input[name=reusableElements]:checked').attr('id');
  var valueRisk = $('#risk :selected').text();
  var valueSavings = $('#fee :selected').text();
  var valueStrategy = $('input[name=strategy]:checked').attr('id');
  var valueUrgent = $('input[name=urgent]:checked').attr('id');
  var valueWorkaround = $('input[name=workaround]:checked').attr('id'); // END parse the value of the input fields
  // ---------------------------------------------------------

  var valueIssueId = getIssueId();
  var option = {};
  option['issueId'] = valueIssueId;
  option['ageOfIssue'] = valueAgeOfIssue;
  option['clientCategory'] = valueClientCategory;
  option['clientsAffected'] = valueClientsAffected;
  option['customersAffected'] = valueCustomersAffected;
  option['customersDisturbed'] = valueCustomersDisturbed;
  option['deadline'] = valueDeadline;
  option['earnings'] = valueEarnings;
  option['earningsProbability'] = valueEarningsProbability; //    option["internalCosts"] = valueInternalCosts;

  option['taskSize'] = valueTaskSize;
  option['issueType'] = valueIssueType;
  option['reusableElements'] = valueReusableElements;
  option['risk'] = valueRisk;
  option['fee'] = valueFee;
  option['strategy'] = valueStrategy;
  option['urgent'] = valueUrgent;
  option['workaround'] = valueWorkaround;
  option['businessValue'] = calculatedBusinessValue;
  option['importanceScore'] = calculatedImportanceScore;
  option['urgencyScore'] = calculatedUrgencyScore;
  option = JSON.stringify(option); //    console.log(option);

  return option;
} // END getSelectedOptions
// ---------------------------------------------------------
// ---------------------------------------------------------
// START getIssueId


function getIssueId() {
  var issueId = $('#issueId input').val();
  return issueId;
} // END getIssueId
/// ---------------------------------------------------------
// ---------------------------------------------------------
// START saveResultstoJSON 
// save results into a new json file on the CLS server


function saveResultstoJSON() {
  $BVTResultsJson = getSelectedOptions();
  $.ajax({
    type: 'POST',
    dataType: 'json',
    url: '/sandbox/php/BVT_dbconnector.php?mode=save-bvt&tool=BVT',
    data: $BVTResultsJson,
    statusCode: {
      400: function () {
        alert('Eek, something went wrong! (error 400 - BVT_dbconnector). Please contact Anne Schwarz.');
      },
      200: function () {
        showDialog('Results saved succesfully! :)');
      },
      500: function () {
        alert('Eek, something went wrong! (error 500). Please contact Anne Schwarz.');
      }
    }
  });
  console.log('saveResultstoJSON');
  JiraUpdates($BVTResultsJson);
} // END saveResultsToJSON
// ---------------------------------------------------------
// END saveResults	
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//var dummy = jQuery.parseJSON('{"issueId":"CLS-1212","clientCategory":"Prospect","customersAffected":"10 - 99","strategy":"fullAlignment"}');


var dummy = jQuery.parseJSON('{"ageOfIssue":">20 days","clientCategory":"[Internal project]","clientsAffected":">99","customersAffected":">10,000","customersDisturbed":"false","deadline":"3 - 5 days","earnings":"> 20,000 Ã¢â€šÂ¬","earningsProbability":"earningsProbabilityLow","taskSize":"3XL","issueId":"2222","issueType":"feature","reusableElements":"false","risk":"No risk","savings":"10.000 - 99.999 EUR","strategy":"fullAlignment","urgent":"urgentFalse","workaround":"workaroundFalse"}');

function loadData() {
  // get all key - value pairs 
  $.each(dummy, function (key, value) {
    // type is SELECT (correct) or DIV (wrong - actually it's a radio within a wrapping div)        
    var type = $('#' + key).prop('tagName'); //var typeRadio = $("#"+value).prop('tagName');  
    //console.log(type);
    // fill all selects with the stored values - WORKS

    if (type == 'SELECT') {
      var key = document.getElementById(key); //$("#"+key+" option[value='20']").prop('selected',true);
      // iterate through all keys of element type select and search for the option which contains the value in the text
      // e.g.: "customersAffected":"10 - 99"

      for (var i = 0; i < key.options.length; i++) {
        if (key.options[i].text == value) {
          key.selectedIndex = i;
          $(key).trigger('change');
          break;
        }
      }
    } // type <div>
    else if (type == 'DIV') {
        /* 
         <div id="strategy">
         <p>
         <label for="fullAlignment" class="ui-checkboxradio-label ui-corner-all ui-button ui-widget ui-checkboxradio-radio-label">
         <span class="ui-checkboxradio-icon ui-corner-all ui-icon ui-icon-background ui-icon-blank"></span>
         <span class="ui-checkboxradio-icon-space"> </span>
         Yes
         </label>
         <input type="radio" name="strategy" value="15" id="fullAlignment" class="inputOptions ui-checkboxradio ui-helper-hidden-accessible">
         </p>
         <i class="icon"><i class="icon"></i></div>
         */
        // check all child elements of that div for the element with the ID = value 
        //$('div#'+key).has('input#'+value).css("background-color", "red");
        // remove checked attribute for all children of this div 
        $('div#' + key + 'input').each(function () {
          $(this).prop('checked', false);
        });
        $('div#' + key + ' input#' + value).each(function () {
          $(this).prop('checked', true);
        }); // add checked="checked" to this element and remove it from all other child elements of this div
        //var key2 =  document.getElementsByName(key2).innerHTML;
        //console.log(key2);
        // var key = document.getElementsByName(key);
        //console.log(key + value);
        // check array of all elements where name = key for the one with id = value
      }
  });
} // ---------------------------------------------------------
// START visualizeCriticality
// **********************************
// START function visualizeCriticality


function visualizeCriticality(htmlid) {
  var selectOption = $('#' + htmlid + ' option:selected').text(); // count how many options are available

  var selectSize = $('#' + htmlid + ' option').size(); // count which of the options has been selected

  var savedIndex = parseInt($('#' + htmlid).prop('selectedIndex'), 10); // add +1 since the counting starts with 0

  var savedIndex = savedIndex + 1; // transform the selected option into a value between 0 and 100% and write it into the attribute aria-valuenow

  var globalProgressbarValueNow = parseInt($('#' + htmlid + '-progressbar').attr('aria-valuenow'), 10); // translate the selected option into a percentage value

  var percentage = savedIndex / selectSize * 100; // depending on the percentage, style the bar: width is set by style attribute, color by class

  var progressbar = percentage;

  if (selectOption === 'UNKNOWN') {
    $('#' + htmlid + '-progressbar').css('width', '100%').attr('aria-valuenow', 100).removeClass().addClass('progress-bar progress-bar-info progress-bar-striped');
    $('#' + htmlid + '-progressbar-inverse').css('width', '100%').attr('aria-valuenow', 100).removeClass().addClass('progress-bar progress-bar-info progress-bar-striped');
  } else if (percentage <= 32) {
    $('#' + htmlid + '-progressbar').css('width', progressbar + '%').attr('aria-valuenow', progressbar).removeClass().addClass('progress-bar progress-bar-success');
    $('#' + htmlid + '-progressbar-inverse').css('width', progressbar + '%').attr('aria-valuenow', progressbar).removeClass().addClass('progress-bar progress-bar-danger');
  } else if (percentage >= 33 && percentage <= 65) {
    $('#' + htmlid + '-progressbar').css('width', progressbar + '%').attr('aria-valuenow', progressbar).removeClass().addClass('progress-bar progress-bar-warning');
    $('#' + htmlid + '-progressbar-inverse').css('width', progressbar + '%').attr('aria-valuenow', progressbar).removeClass().addClass('progress-bar progress-bar-warning');
  } else if (percentage >= 66) {
    $('#' + htmlid + '-progressbar').css('width', progressbar + '%').attr('aria-valuenow', progressbar).removeClass().addClass('progress-bar progress-bar-danger');
    $('#' + htmlid + '-progressbar-inverse').css('width', progressbar + '%').attr('aria-valuenow', progressbar).removeClass().addClass('progress-bar progress-bar-success');
  }
}

; // **********************************
// END visualizeCriticality
// ---------------------------------------------------------

/*Navigation*/

function openNav() {
  document.getElementById('mySidenav').style.width = '250px'; //    document.getElementById("main").style.marginLeft = "250px";
  //    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
  document.getElementById('mySidenav').style.width = '0'; //    document.getElementById("main").style.marginLeft= "0";
  //    document.body.style.backgroundColor = "white";
}

function JiraUpdates($BVTResultsJson) {
  //	$json = getSelectedOptions();
  var obj = JSON.parse($BVTResultsJson);
  $issueId = obj.issueId;
  $businessValue = obj.businessValue;
  $.ajax({
    type: 'GET',
    url: '/sandbox/php/Jira_connector.php?issueId=' + $issueId + '&businessValue=' + $businessValue,
    statusCode: {
      400: function () {
        alert('Eek, something went wrong! (error 400 - Jira_connector). Please contact Anne Schwarz.');
      },
      200: function () {
        showDialog('Results saved succesfully! :)');
      },
      500: function () {
        alert('Eek, something went wrong! (error 500). Please contact Anne Schwarz.');
      }
    }
  });
}

;