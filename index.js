

// get tickets 
// get tickets lables 
// calculate value based on input 
// update new custom field in JIRA to show new value assigned 
// sort swim lane based on value given to each ticket 

require('dotenv').config()
const Axios = require("axios")
const JiraService = require("./services/JiraService") 
const ProcessTickets = require( "./services/ProcessTickets")

// define class 
class App {
    constructor(){
        this.axios = Axios
        this.jiraService = new JiraService(this.axios)
        this.ticketProcess = new ProcessTickets();
    }

    async init(){
        /* set up services and configurations */ 
        

        // test cases 
        const testUser = "komalley"
        const testProjectKey = "CLX"
        const testSingleTicketId = "CLX-7769"
        //console.log( this.jiraService.getUser(testUser))
        // console.log(this.jiraService.getSingleTicket(testSingleTicketId))
        
        const singleTicket = await this.jiraService.getSingleTicket(testSingleTicketId)
        // console.log(singleTicket)
        const labelsFromSingleTicket = this.ticketProcess.getLablesFromSingleTicket(singleTicket)
        console.log("labels From Single Ticket", labelsFromSingleTicket)
    }
}


// call from the console
(()=>{

    const application = new App();
    application.init()
})()


// export a function to be used as npm module - simply return a new instance of the object all ready bootstrapped
module.exports = ()=>{
    
    const Application = new App()
    Application.init()
    return Application;
}