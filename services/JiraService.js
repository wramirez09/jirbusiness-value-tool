

const queries = require("../queries/queries")

class JiraService {
    constructor(axios){
        this.axios = axios
        // test url must replace 
        this.jira_endpoint = process.env.jira_endpoint ? process.env.jira_endpoint : "https://jira.cgn.cleverbridge.com/rest/api";
        
        this.username = process.env.jira_username;
        this.password = process.env.jira_password
        this.authorization = process.env.jira_authBase64
        this.requestOptions = (queryString)=>{

            return {
                method: 'GET',
                headers: { 'Authorization': this.authorization },
                url: this.jira_endpoint + queryString
            }
        }
    }

    init() {
        console.log("bootstrap jira service")

        /** TO DO's */
        // METHODS TO WRITE 
        // -> query all tickets in project
        // -> query single ticket by
        // - get labels from ticket 
    }


    async apiRequest(optionsObject){

        return await this.axios(optionsObject).catch((error)=>{console.log("connection api error", error)});
    }

    async createOptionsObjectAndMakeCall(requestQuery){
        const options = this.requestOptions(requestQuery)
        const data = await this.apiRequest(options)

        return data
    }

    // test method used to see data
   async getUser(user){
        const requestQuery = queries.getTicketsByUserQuery(user)
        const data = await this.createOptionsObjectAndMakeCall(requestQuery)
        console.log("user tickets", data);
        return data
    }

    async getByProjectKey(projectKey){

        const requestQuery = queries.getAllTicketsByProjectKeyQuery(projectKey)
        const data = await this.createOptionsObjectAndMakeCall(requestQuery)
        console.log("project based tickets", data);
        return data
    }

    async getSingleTicket(ticketID){
        const requestQuery = queries.getSingleTicket(ticketID)
        const options = this.requestOptions(requestQuery)
        const data = await this.apiRequest(options)
        //console.log("project based tickets", data);
        return data
    }
}


module.exports = JiraService