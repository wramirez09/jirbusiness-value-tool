

const queries = {

    // get jira tickets by user 

    getTicketsByUserQuery(user){

        return `/latest/user?username=${user}`
    },

    // get all jira tickets by project
    getAllTicketsByProjectKeyQuery(projectKey){

        return `/2/search?jql=project=${projectKey}`

    },

    // get single Jira ticket
    getSingleTicket(ticketID){

        return `/latest/issue/${ticketID}/`
    }


}


module.exports = queries